import Button from '../../components/Button';

import { useTranslation } from 'react-i18next';

const Home = () => {
  const { t } = useTranslation();

  const onBtnHelloClick = () => {
      alert('Hello world');
  }

  return (
    <div className='home'>
      <Button style={{marginRight: '.5rem'}} onClick={onBtnHelloClick} label={t('hello')}></Button>
      <Button type='secondary' onClick={onBtnHelloClick} label={t('hello')}></Button>
    </div>
  );
};

export default Home;
