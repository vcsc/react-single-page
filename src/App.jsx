import './App.css';
import AppRoutes from './routers';

function App() {
  return (
    <div className='App'>
      <AppRoutes></AppRoutes>
    </div>
  );
}

export default App;
