import classNames from 'classnames';
import { ButtonStyle } from './style';

const Button = ({ label = 'Button', type = 'primary', onClick = null, ...rest }) => {
  const btnClass = classNames({
    btn: true,
    'btn-primary': type === 'primary',
    'btn-secondary': type === 'secondary',
  });

  const handleOnClick = () => {
    if (onClick) {
      onClick();
    }
  };

  return (
    <button onClick={handleOnClick} className={btnClass} {...rest} >
      {label}
      <style jsx>{ButtonStyle}</style>
    </button>
  );
};

export default Button;
