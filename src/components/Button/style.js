import css from 'styled-jsx/css';

export const ButtonStyle = css`
    .btn {
        min-height: 2rem;
        cursor: pointer;
        outline: none;
        border: none;
        padding: .5rem;
        border-radius: .2rem;
    }

    .btn-primary {
        background-color: blue;
        color: white;
    }

    .btn-secondary {
        background-color: black;
        color: white;
    }

`