import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from '../containers/Home';

const AppRoutes = () => {
  return (
    <Router>
      <Switch>
        <Route path='/'>
          <Home></Home>
        </Route>
      </Switch>
    </Router>
  );
};

export default AppRoutes;
